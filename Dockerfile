from python:3.7-slim

RUN pip install requests==2.22.0 influxdb==5.2.3

ADD . /app

ENV PYTHONPATH=/app
WORKDIR /app
