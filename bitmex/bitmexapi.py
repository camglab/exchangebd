import requests
import enum
import logging


class Currency(enum.Enum):

    BTC = "XBT"
    TRX = "TRX"
    EOS = "EOS"
    LTC = "LTC"
    BCH = "BCH"
    XRP = "XRP"
    ETH = "ETH"
    TOTAL = "Total"


class BitmexEndpoint(enum.Enum):

    Stats = "/stats"
    StatsHistory = "/stats/history"
    StatsHistoryUSD = "/stats/historyUSD"


class BitmexAPI(object):

    def __init__(self, host="https://www.bitmex.com/api/v1"):
        self.host = host

    def get(self, url, params):
        logging.info(f"GET {url} {params}")
        resp = requests.get(url, params=params)
        if resp.status_code == 200:
            return resp.json()
        else:
            raise requests.HTTPError(f"GET {url} {params} status_code: {resp.status_code} response: {resp.text}")
    
    def get_endpoint(self, endpoint: BitmexEndpoint, params: dict=None):
        if not isinstance(params, dict):
            params = {}
        url = f"{self.host}{endpoint.value}"
        return self.get(url, params)
    


