from bitmexapi import Currency, BitmexAPI, BitmexEndpoint
from influxdb import InfluxDBClient
from datetime import datetime
import traceback
import logging
import os


logging.basicConfig(
    datefmt="%Y-%m-%dT%H:%M:%S",
    format="%(asctime)s | %(message)s"
)


VALID_SYMBOLS= set(map(lambda e: e.value, Currency))

def init_client_from_env():
    return InfluxDBClient(
        os.environ.get("INFLUXDB_HOST", "localhost"),
        int(os.environ.get("INFLUXDB_PORT", "8086")),
        os.environ.get("INFLUXDB_USERNAME", "root"),
        os.environ.get("INFLUXDB_PASSWORD", "root"),
        os.environ.get("INFLUXDB_DATABASE", None)
    )


def main():
    api = BitmexAPI()
    client = init_client_from_env()
    update_oi(api, client)

def history():
    import pandas as pd
    table = pd.read_csv("oi.csv", index_col="time")
    stack = table.pct_change().stack()
    print(stack[stack!=0].describe())


def update_oi(api: BitmexAPI, client: InfluxDBClient):
    measurement = f"Bitmex{BitmexEndpoint.Stats.name}"
    latest_total_value = get_latest_total_value(client, measurement, Currency.TOTAL.name, "openValue")
    logging.warning(f"[update {BitmexEndpoint.Stats}] latest total value = {latest_total_value}")
    data = api.get_endpoint(BitmexEndpoint.Stats)
    total_open_value = get_current_total_value(data)
    logging.warning(f"[update {BitmexEndpoint.Stats}] current total value = {total_open_value}")
    if total_open_value != latest_total_value:
        write_oi(client, data, measurement)


def write_oi(client: InfluxDBClient, data: list, measurement: str):
    now = datetime.now()
    points = []
    for record in data:
        symbol = record["rootSymbol"]
        if symbol in VALID_SYMBOLS:
            logging.warning(f"[update {BitmexEndpoint.Stats}] record: {record}")
            point = {
                "measurement": measurement,
                "time": now,
                "tags": {
                    "currency": Currency(symbol).name, 
                    "period": "m5"
                },
                "fields": {
                    "openValue": record["openValue"],
                    "openInterest":  record["openInterest"] if record["openInterest"] else 0
                }
            }
            points.append(point)
    result = client.write_points(points)
    logging.warning(f"[update {BitmexEndpoint.Stats}] write {len(points)} {result}")
    return result

def get_current_total_value(data):
    for pos in reversed(range(len(data))):
        if data[pos]["rootSymbol"] == Currency.TOTAL.value:
            return data[pos]["openValue"]
    return 0


def get_latest_total_value(client: InfluxDBClient, measurement: str, currency: str, field: str):
    result = client.query(f"select {field} from {measurement} where currency='{currency}' order by time desc limit 1")
    if result:
        return list(result.get_points(measurement))[0][field]
    else:
        return 0



if __name__ == "__main__":
    main()
    