import requests
import enum
import logging
import re


PCOMPILER = re.compile("([m|h|d])([0-9]{1,2})")
MINUTE_COUNT = {
    "m": 1,
    "h": 60,
    "d": 60*24
}


def period_minutes(period: str):
    p, n = PCOMPILER.match(period).groups()
    return int(int(n) * MINUTE_COUNT[p])

class Currency(enum.Enum):

    BTC = "BTCUSDT"
    ETH = "ETHUSDT"
    ETC = "ETCUSDT"
    EOS = "EOSUSDT"
    BCH = "BCHUSDT"
    LTC = "BTCUSDT"
    XRP = "XRPUSDT"
    TRX = "TRXUSDT"
    

class BinanceFutureData(enum.Enum):

    OpenInterestHist = "openInterestHist"
    TopLongShortAccountRatio = "topLongShortAccountRatio"
    TopLongShortPositionRatio = "topLongShortPositionRatio"
    GlobalLongShortAccountRatio = "globalLongShortAccountRatio"


class BinancePublicFutureData(enum.Enum):

    OpenInterestHist = "open-interest-stats"
    TopLongShortAccountRatio = "long-short-account-ratio"
    TopLongShortPositionRatio = "long-short-position-ratio"
    GlobalLongShortAccountRatio = "global-long-short-account-ratio"
    TakerLongShortRatio = "taker-long-short-ratio"


class Period(enum.Enum):

    m5 = "5m"
    m15 = "15m"
    m30 = "30m"
    h1 = "1h"
    h2 = "2h"
    h4 = "4h"
    h6 = "6h"
    h12 = "12h"
    d1 = "1d"


class BinanceFutureAPI(object):

    def __init__(self, host="https://fapi.binance.com/futures/data"):
        self.host = host
    
    def get(self, url, params):
        logging.info(f"GET {url} {params}")
        resp = requests.get(url, params=params)
        if resp.status_code == 200:
            return resp.json()
        else:
            raise requests.HTTPError(f"GET {url} {params} status_code: {resp.status_code} response: {resp.text}")
    
    def get_future_data(self, datatype: BinanceFutureData, currency: Currency, period: Period, limit: int=None, startTime: int=None, endTime: int=None):
        params = {
            "symbol": currency.value,
            "period": period.value
        }
        if limit:
            params["limit"] = limit
        if startTime:
            params["startTime"] = startTime
        if endTime:
            params["endTime"] = endTime
        
        url = f"{self.host}/{datatype.value}"
        result = self.get(url, params)
        return result


import json


class BinancePublicFutureAPI(object):

    def __init__(self, host="https://www.binance.com/gateway-api/v1/public/future/data"):
        self.host = host
    
    def post(self, url, params):
        logging.info(f"POST {url} {params}")
        resp = requests.post(url, json=params)
        if resp.status_code == 200:
            return resp.json()
        else:
            raise requests.HTTPError(f"POST {url} {params} status_code: {resp.status_code} response: {resp.text}")
    
    def post_public_data(self, datatype: BinancePublicFutureData, currency: Currency, period: Period):
        params = {
            "name": currency.value,
            "periodMinutes": period_minutes(period.name)
        }
        
        url = f"{self.host}/{datatype.value}"
        result = self.post(url, params)
        if result.get("success", False):
            return result['data']
        else:
            raise ValueError(f"Result not success: {result}")