from binanceapi import BinanceFutureAPI, BinanceFutureData, Currency, Period, period_minutes, BinancePublicFutureAPI, BinancePublicFutureData
from influxdb import InfluxDBClient
from datetime import datetime
from itertools import product
import logging
import os
import traceback

logging.basicConfig(
    datefmt="%Y-%m-%dT%H:%M:%S",
    format="%(asctime)s | %(message)s"
)


FUTURE_DATA_SIZE_DEFAULT = 30
FUTURE_DATA_EXPIRE = int(30*24*3600)

def init_client_from_env():
    return InfluxDBClient(
        os.environ.get("INFLUXDB_HOST", "localhost"),
        int(os.environ.get("INFLUXDB_PORT", "8086")),
        os.environ.get("INFLUXDB_USERNAME", "root"),
        os.environ.get("INFLUXDB_PASSWORD", "root"),
        os.environ.get("INFLUXDB_DATABASE", None)
    )



def period_seconds(period: str):
    return int(period_minutes(period)*60)


def get_db_latest(client: InfluxDBClient, measurement: str, currency: str, period: str):
    query = f"select * from {measurement} where currency='{currency}' and period='{period}' order by time desc limit 1"
    result = client.query(query, epoch="s")
    if result:
        record = list(result.get_points(measurement))
        return record[0]["time"]
    else:
        return 0
    

def update_future_data(api: BinanceFutureAPI, client: InfluxDBClient, datatype: BinanceFutureData, currency: Currency, period: Period):
    measurement = f"Binance{datatype.name}"
    now = datetime.now().timestamp()
    pduration = period_seconds(period.name)
    ctime = now - now % pduration
    ltime = get_db_latest(client, measurement, currency.name, period.name)
    logging.warning(f"[update future] [{datatype.name} {currency.name} {period.name}] [database latest time] {ltime}")
    if not ltime:
        ltime = ctime - FUTURE_DATA_EXPIRE
    if ltime >= ctime:
        logging.warning(f"[update future] [{datatype.name} {currency.name} {period.name}] [data updated] latest={ltime} current={ctime}")
        return ltime
    etime = ltime + FUTURE_DATA_SIZE_DEFAULT*pduration
    params = {
        "datatype": datatype,
        "currency": currency,
        "period": period,
        "startTime": int(ltime*1000),
        "endTime": int(etime*1000)
    }
        
    logging.warning(f"[update future] [query binance] {params}")
    data = api.get_future_data(**params)
    if len(data):
        result = write_future_data(
            client,
            measurement,
            {"currency": currency.name, "period": period.name},
            data
        )
        if not result:
            raise ValueError(f"[update future] [write data failed] params={params} latestdata={data[-1]}")
        last = data[-1]["timestamp"]/1000
        if last >= ctime:
            return last
        elif last <= ltime:
            return ltime
        else:
            return update_future_data(api, client, datatype, currency, period)
            
    else:
        logging.warning(f"[update future] [empty response] params={params}")
        return ltime


def write_future_data(client: InfluxDBClient, measurement: str, tags: dict, data:list):
    points = [make_future_data_point(record.copy(), measurement, tags) for record in data]
    logging.warning(f"[write data] {points[0]} ~ {points[-1]}")
    return client.write_points(points, time_precision="ms")


def make_future_data_point(data, measurement, tags):
    data.pop("symbol")
    t = int(data.pop("timestamp"))
    for key, value in list(data.items()):
        data[key] = float(value)
    return {
        "measurement": measurement,
        "tags": tags,
        "time": t,
        "fields": data
    }


def future_update_all(api: BinanceFutureAPI, client: InfluxDBClient):
    for datatype, currency, period in product(BinanceFutureData, Currency, Period):
        logging.warning(f"[update future] [{datatype} {currency} {period}] start")
        try:
            ltime = update_future_data(api, client, datatype, currency, period)
        except KeyboardInterrupt:
            break
        except:
            logging.error(f"[update future] [{datatype} {currency} {period}] {traceback.format_exc()}")
        else:
            logging.warning(f"[update future] [{datatype} {currency} {period}] latest time: {datetime.fromtimestamp(ltime)}")


def future_update_routine(api: BinanceFutureAPI, client: InfluxDBClient):
    now = datetime.now().timestamp()
    current = now - now % 60
    for period in Period:
        seconds = period_seconds(period.name)
        if current % seconds == 0:
            logging.warning(f"[update future] [{period}] ")
            for datatype, currency in product(BinanceFutureData, Currency):
                logging.warning(f"[update future] [{datatype} {currency} {period}] start")
                try:
                    ltime = update_future_data(api, client, datatype, currency, period)
                except KeyboardInterrupt:
                    break
                except:
                    logging.error(f"[update future] [{datatype} {currency} {period}] {traceback.format_exc()}")
                else:
                    logging.warning(f"[update future] [{datatype} {currency} {period}] latest time: {datetime.fromtimestamp(ltime)}")


def make_public_data_points(data, measurement, tags):
    times = data["xAxis"]
    names = [records["name"].replace("/", "").replace(" ","") for records in data["series"]]
    arrays = [records["data"] for records in data["series"]]
    fields = map(lambda item: dict(zip(names, item)), zip(*arrays))
    return [{"time": time, "measurement": measurement, 'tags': tags, "fields": field} for time, field in zip(times, fields)]


def update_public_data(api: BinancePublicFutureAPI, client: InfluxDBClient, datatype: BinancePublicFutureData, currency: Currency, period: Period):
    measurement = f"Binance{datatype.name}"
    data = api.post_public_data(
        datatype,
        currency,
        period
    )
    tags = {"currency": currency.name, "period": period.name}
    points = make_public_data_points(data, measurement, tags)
    if points:
        r = client.write_points(points, time_precision="ms")
        logging.warning(f"[public data] [{datatype} {currency} {period}] write: {r} {points[0]['time']} ~ {points[-1]['time']}")
    else:
        logging.warning(f"[public data] [{datatype} {currency} {period}] data is empty.")


def public_update_routine(api: BinancePublicFutureAPI, client: InfluxDBClient):
    now = datetime.now().timestamp()
    current = now - now % 60
    for period in Period:
        seconds = period_seconds(period.name)
        if current % seconds == 0:
            logging.warning(f"[update public] [{period}] ")
            datatype = BinancePublicFutureData.TakerLongShortRatio
            for currency in Currency:
                logging.warning(f"[update public] [{datatype} {currency} {period}] start")
                try:
                    update_public_data(api, client, datatype, currency, period)
                except KeyboardInterrupt:
                    break
                except:
                    logging.error(f"[update public] [{datatype} {currency} {period}] {traceback.format_exc()}")



def test_public():
    api = BinancePublicFutureAPI()
    client = init_client_from_env()
    # update_public_data(api, client, BinancePublicFutureData.TakerLongShortRatio, Currency.BTC, Period.m5)
    public_update_routine(api, client)


def main():
    import sys
    client = init_client_from_env()
    api = BinanceFutureAPI()

    method = sys.argv[1]
    if method == "all":
        future_update_all(api, client)
    elif method == 'routine':
        future_update_routine(api, client)
        public_update_routine(BinancePublicFutureAPI(), client)


if __name__ == "__main__":
    main()