from influxdb import InfluxDBClient
import os
from huobiapi import HuobiApi, Symbol, Period, ContractType, OpenInterestUnit, ELITE_PERIODS, CONTRACT_PERIODS
from itertools import product
import logging
import re
import traceback
from datetime import datetime


logging.basicConfig(
    datefmt="%Y-%m-%dT%H:%M:%S",
    format="%(asctime)s | %(message)s"
)


PCOMPILER = re.compile("([m|h|d])([0-9]{1,2})")
P_GROUP = {
    "m": 60,
    "h": 3600,
    "d": 24*3600
}


def periodDuration(period: str):
    p, n = PCOMPILER.match(period).groups()
    return int(P_GROUP[p]*int(n))


def make_points(data, measurement, time_index, tags):
    return [
        {   
            "measurement": measurement,
            "time": doc.pop(time_index),
            "tags": tags,
            "fields": doc
        } for doc in data
    ]


def init_client_from_env():
    return InfluxDBClient(
        os.environ.get("INFLUXDB_HOST", "localhost"),
        int(os.environ.get("INFLUXDB_PORT", "8086")),
        os.environ.get("INFLUXDB_USERNAME", "root"),
        os.environ.get("INFLUXDB_PASSWORD", "root"),
        os.environ.get("INFLUXDB_DATABASE", None)
    )


def update_contract_elite_account_ratio(api: HuobiApi, client: InfluxDBClient, symbol: Symbol, period: Period):
    measurement = "HuobiEliteAccountRatio"
    tags = {
        "currency": symbol.name,
        "period": period.name
    }
    logging.warning(f"[{measurement} {symbol} {period}] query")
    data = api.contract_elite_account_ratio(symbol, period)
    points = make_points(data, measurement, "ts", tags)
    if len(points):
        logging.warning(f"[{measurement} {symbol} {period}] {points[0]} ~ {points[-1]}")
        r = client.write_points(points, time_precision="ms")
        logging.warning(f"[{measurement} {symbol} {period}] write {r}")
    else:
        logging.warning(f"[{measurement} {symbol} {period}] empty data")


def update_contract_elite_position_ratio(api: HuobiApi, client: InfluxDBClient, symbol: Symbol, period: Period):
    measurement = "HuobiElitePositionRatio"
    tags = {
        "currency": symbol.name,
        "period": period.name
    }
    logging.warning(f"[{measurement} {symbol} {period}] query")
    data = api.contract_elite_position_ratio(symbol, period)
    points = make_points(data, measurement, "ts", tags)
    if len(points):
        logging.warning(f"[{measurement} {symbol} {period}] {points[0]} ~ {points[-1]}")
        r = client.write_points(points, time_precision="ms")
        logging.warning(f"[{measurement} {symbol} {period}] write {r}")
    else:
        logging.warning(f"[{measurement} {symbol} {period}] empty data")


def update_contract_his_open_interest(api: HuobiApi, client: InfluxDBClient, symbol: Symbol, contract_type: ContractType, period: Period, unit: OpenInterestUnit):
    measurement = "HuobiOpenInterest"
    tags = {
        "currency": f"{symbol.name}_{contract_type.name.upper()}",
        "period": period.name
    }
    logging.warning(f"[{measurement} {symbol} {contract_type} {period}] query")
    data = api.contract_his_open_interest(
        symbol,
        contract_type,
        period,
        200,
        unit
    )
    data = [{"ts": doc["ts"], unit.name: float(doc["volume"])} for doc in data]
    points = make_points(data, measurement, "ts", tags)
    if len(points):
        logging.warning(f"[{measurement} {symbol} {period}] {points[0]} ~ {points[-1]}")
        r = client.write_points(points, time_precision="ms")
        logging.warning(f"[{measurement} {symbol} {period}] write {r}")
    else:
        logging.warning(f"[{measurement} {symbol} {period}] empty data")


def routing():
    client = init_client_from_env()
    api = HuobiApi()
    now = datetime.now().timestamp()
    now = now - now % 60
    for period in ELITE_PERIODS:
        duration = periodDuration(period.name)
        if now % duration == 0:
            logging.warning(f"Update {period} elite data")
            for symbol in Symbol:
                try:
                    update_contract_elite_account_ratio(api, client, symbol, period)
                except:
                    logging.warning(traceback.format_exc())

                try:
                    update_contract_elite_position_ratio(api, client, symbol, period)
                except:
                    logging.warning(traceback.format_exc())
    
    for period in CONTRACT_PERIODS:
        duration = periodDuration(period.name)
        if now % duration == 0:
            logging.warning(f"Update {period} OpenInterest")
            for symbol, contract_type, unit in product(Symbol, ContractType, OpenInterestUnit):
                try:
                    update_contract_his_open_interest(api, client, symbol, contract_type, period, unit)
                except:
                    logging.warning(traceback.format_exc())





if __name__ == "__main__":
    routing()