import requests
import enum


class Symbol(enum.Enum):

    BTC = "BTC"
    ETH = "ETH"
    EOS = "EOS"
    BCH = "BCH"
    BSV = "BSV"
    LTC = "LTC"
    XRP = "XRP"
    ETC = "ETC"
    TRX = "TRX"


class ContractType(enum.Enum):
    
    this_week = "this_week"
    next_week = "next_week"
    quarter = "quarter"


class Period(enum.Enum):
    m5 = "5min"
    m15 = "15min"
    m30 = "30min"
    h1 = "60min"
    h4 = "4hour"
    h12 = "12hour"
    d1 = "1day"


class OpenInterestUnit(enum.Enum):

    cont = 1
    cryptocurrency = 2


ELITE_PERIODS = {
    Period.m5,
    Period.m15,
    Period.m30,
    Period.h1,
    Period.h4,
    Period.d1,
}


CONTRACT_PERIODS = {
    Period.h1,
    Period.h4,
    Period.h12,
    Period.d1
}


class HuobiApi(object):

    def __init__(self, host="https://api.hbdm.com"):
        self.host = host

    def get(self, path, params):
        url = f"{self.host}{path}"
        resp = requests.get(url, params=params)
        if resp.status_code == 200:
            return resp.json()
        else:
            raise requests.HTTPError(f"GET {url} {params} status_code: {resp.status_code} response: {resp.text}")

    def contract_elite_account_ratio(self, symbol: Symbol, period: Period):
        assert period in ELITE_PERIODS, f"Invalid period: {period} for elite data. Valid periods: {ELITE_PERIODS}"
        params = {
            "symbol": symbol.value,
            "period": period.value
        }
        result = self.get(
            "/api/v1/contract_elite_account_ratio",
            params
        )
        if result.get("status", "") == "ok":
            return result["data"]["list"]
        else:
            raise ValueError("Result status error: {result}")
    
    def contract_elite_position_ratio(self, symbol: Symbol, period: Period):
        assert period in ELITE_PERIODS, f"Invalid period: {period} for elite data. Valid periods: {ELITE_PERIODS}"
        params = {
            "symbol": symbol.value,
            "period": period.value
        }
        result = self.get(
            "/api/v1/contract_elite_position_ratio",
            params
        )
        if result.get("status", "") == "ok":
            return result["data"]["list"]
        else:
            raise ValueError("Result status error: {result}")
    
    def contract_his_open_interest(self, symbol: Symbol, contract_type: ContractType, period: Period, size: int=48, amount_type: OpenInterestUnit=OpenInterestUnit.cont):
        assert period in CONTRACT_PERIODS, f"Invalid period: {period} for contract hist data. Valid periods: {CONTRACT_PERIODS}"
        assert size>=1 and size<=200, f"size: {size} out of range 1 ~ 200"
        params = {
            "symbol": symbol.value,
            "contract_type": contract_type.value,
            "period": period.value,
            "size": size,
            "amount_type": amount_type.value
        }
        result = self.get(
            "/api/v1/contract_his_open_interest",
            params
        )
        if result.get("status", "") == "ok":
            return result["data"]["tick"]
        