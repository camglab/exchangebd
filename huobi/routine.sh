#! /bin/bash


while true
do
    date
    python storage.py

    minute=`date +%M`
    second=`date +%S`
    seconds=`expr 300 - \( $minute % 5 \) \* 60 - $second` 
    sleep $seconds
done